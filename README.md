# GoCyberto

Use GoCyberto to avoid clear text passwords.
A description of the problem is here:
https://www.linkedin.com/pulse/how-avoid-clear-text-passwords-user-names-server-scripts-oliver-glas/

Check out this wiki page for a detailed description of the tool:
https://gocyberto.inss.ch/Main_Page

It is still in beta phase.

## LINUX Debian
There is a deb package for Debian based systems (Ubuntu, Debian).

## Linux RedHat
There is a rpm package for RedHat based systems (CentOS, Fedora).

## Linux other and Mac OS
You can copy the binary directly to the /usr/local/bin folder.
Linux
sudo mv gocyberto_lnx /usr/local/bin/gocyberto
Mac OS
sudo mv gocyberto_mac /usr/local/bin

gocyberto_lnx is Linux 64bit architecture.
gocyberto_lnx386 is Linux 32bit architecture.
gocyberto_mac is mac OS architecture.

## Windows 
There is an installation file (SetupGoCyberto.exe) for the binary.
It contains also a start.bat file to open a cmd windows.
GoCyberto is a pure command line tool.
You should add the binary path to the PATH system variable.

## Start
Check if it works with 
 gocyberto
which shall give you the welcome message.

This is a free demo version which will expire at 01.08.2021.

Please contact info@inss.ch for a production license.





